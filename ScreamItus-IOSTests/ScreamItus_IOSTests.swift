//
//  ScreamItus_IOSTests.swift
//  ScreamItus-IOSTests
//
//  Created by Joseph John on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

class ScreamItus_IOSTests: XCTestCase {
    // create a global variable for ScreamItus
    var infection: ScreamItus_IOSTests!

    override func setUp() {
        // initialise the global variable
        infection = ScreamItus_IOSTests();
    }

    override func tearDown() {
        // close the infection
        infection = nil
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
